//
//  Contants.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import Foundation

struct Constants {
    
    struct URLs {
        static let newsUrl = "https://newsapi.org/v2/top-headlines?country=us&apiKey=efc9601566254d9a97ea38c7b9d2def5"
        static let newslikes = "https://cn-news-info-api.herokuapp.com/likes/"
        static let newsComment = "https://cn-news-info-api.herokuapp.com/comments/"
    }
    
}
