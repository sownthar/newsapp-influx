//
//  NewsViewController.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import UIKit

class NewsViewController: UIViewController {
    
    // IBOutlet weak outlets
    @IBOutlet weak var newstableView : UITableView!
    
    //viewmodel value update aftersetting immeditely
    private var articleListVM: ArticleListViewModel! {
        didSet {
            DispatchQueue.main.async {
                self.newstableView.reloadData()
            }
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "News"
        
        // setupViews delegated from tableview
        self.setupViews()
       // setup the Api call to get data
        self.setup()
    }
    
    func setupViews(){
        newstableView.dataSource = self
        newstableView.delegate = self
        newstableView.estimatedRowHeight = 100
        newstableView.registerNib(cell: NewsHeadlineTableViewCell.self)
    }
    
    func setup(){
        
        Webservice().getNewsArticles(for: NewsModel.self, endpoint: URL.urlForHeadllines(), completionHandler: {
            result in
            
            switch (result ){
            case .success(let newsModel):
                //setting the values to viewModel after the api call
                if let articles = newsModel.articles{
                        self.articleListVM = ArticleListViewModel(articles)
                }
            case .failure(let error):
                    //Alert will show incase any failure
                self.showAlert(title: "Error", message: error.localizedDescription, customView: false)
                break
                
            }
        })
    }
}
extension NewsViewController : UITableViewDelegate,UITableViewDataSource{
     func numberOfSections(in tableView: UITableView) -> Int {
         // checnking the number values in list
              return self.articleListVM == nil ? 0 : self.articleListVM.numberOfSections
        }
        
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.articleListVM.numberOfRowsInSection(section)
        }
        
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(cell: NewsHeadlineTableViewCell.self)
            let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
             cell.layer.cornerRadius = 5
             cell.layer.borderColor = UIColor.lightGray.cgColor
             cell.layer.borderWidth = 1
            cell.configure(for: articleVM)
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewsDetailsViewController") as! NewsDetailsViewController
        vc.articleid =  articleVM.articleId
        vc.articleUrl = articleVM.articleurl
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
   
}

extension UIViewController{
    func showAlert(title:String?, message: String?, customView:Bool?, otherButtons:[String:((UIAlertAction)-> ())]? = nil, cancelTitle: String = "Ok", cancelAction: ((UIAlertAction)-> ())? = nil) {
        //let newTitle = title?.capitalized
        //let newMessage = message
     let alert = UIAlertController(title:title , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))
        
        if otherButtons != nil {
            for key in otherButtons!.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
            }
        }

    }
}
