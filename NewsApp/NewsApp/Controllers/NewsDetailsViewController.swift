//
//  NewsDetailsViewController.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import UIKit
import WebKit

class NewsDetailsViewController: UIViewController {
    
    //Outlets mart
    @IBOutlet weak var webview : UIView!
    @IBOutlet weak var likebtn : UIButton!
    @IBOutlet weak var commentBtn : UIButton!
    
    var articleid = String()
    var articleUrl = String()
    
    var likes : Int = 0 {
        didSet{
            DispatchQueue.main.async {
                self.likebtn.setTitle("\(self.likes) likes", for: .normal)
            }
        }
    }
    var comments : Int = 0{
        didSet{
            DispatchQueue.main.async {
                self.commentBtn.setTitle("\(self.comments) comments", for: .normal)
            }
        }
    }

    var wkWebView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "News In Detail"
        // Api call for to get the likes and commments
        
        self.setupApiCall(articleid)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.preferences.javaScriptEnabled = true
        wkWebView = WKWebView(frame: self.webview.frame,
                              configuration: webConfiguration)
        wkWebView.backgroundColor = .white
        webview.addSubview(wkWebView)
        wkWebView.navigationDelegate = self
       
        wkWebView.frame = webview.frame
        self.loadWebview(urlString: articleUrl)

    }
    
    func setupApiCall(_ articleId : String){
        let dispatchGroup = DispatchGroup()

           dispatchGroup.enter()
        Webservice().getNewsArticles(for: LikesModel.self, endpoint: URL.newsLikes(articleId), completionHandler: {
            result in
            switch (result){
            case .success(let likes):
                // got the like from webdays
                if let like = likes.likes{
                    self.likes = like
                }
                break
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription, customView: false)

                break
            }
            dispatchGroup.leave()

        })
        

           dispatchGroup.enter()   // <<---
        Webservice().getNewsArticles(for: CommentsModel.self, endpoint: URL.newsComment(articleId), completionHandler: {
            result in
            switch (result){
            case .success(let comments):
                // got the like from webdays

                if let comment = comments.comments{
                    self.comments = comment
                }
                break
            case .failure(let error):
                self.showAlert(title: "Error", message: error.localizedDescription, customView: false)
                break
            }
            dispatchGroup.leave()
        })
           dispatchGroup.notify(queue: .main) {
               print("calls completed")
           }
    }
    func loadWebview(urlString: String){
        //let urlstrings = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        if let urlCheck = url{
            let urlReq = URLRequest(url: urlCheck)
            self.wkWebView.load(urlReq)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }

}
extension NewsDetailsViewController: WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.view.layoutIfNeeded()
        wkWebView.frame = webview.frame
    }
    
        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            decisionHandler(.allow)
            
        }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.view.layoutIfNeeded()
        wkWebView.frame = webview.frame
        
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
       
    }
}
