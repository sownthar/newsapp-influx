//
//  Extensions.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import Foundation
import UIKit


extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    func registerNib<T: UITableViewCell>(cell: T.Type) {
        register(UINib(nibName: T.identifier, bundle: nil), forCellReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(cell: T.Type) -> T {
        if let cell = dequeueReusableCell(withIdentifier: T.identifier) as? T {
            return cell
        } else {
            fatalError("Desired UITableViewCell is not registered with UITableView.")
        }
    }
}



extension URL {
    
    static func urlForHeadllines() -> URL {
        return URL(string: Constants.URLs.newsUrl)!
    }
    static func newsLikes(_ articleId : String) -> URL {
        return URL(string: Constants.URLs.newslikes + articleId)!
    }
    static func newsComment(_ articleId : String) -> URL {
        return URL(string: Constants.URLs.newsComment +  articleId)!
    }
    
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
