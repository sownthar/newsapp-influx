//
//  ApiRequest.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import Foundation
import UIKit


class Webservice {
    

    func getNewsArticles<T : Decodable>( for : T.Type,endpoint :URL,completionHandler: @escaping (Result<T,Error>) -> Void) {
               
                 
        do{
            let request = RequestFactory.request(method: .GET,url: endpoint)
                    let dataTask = URLSession.shared.dataTask(with: endpoint)
                    { (data, response, Error) -> Void in

                        //error check
                        guard Error == nil
                        else
                        {
                            completionHandler(.failure(Error as! Error))
                            return
                        }
                        
                        // status code
                        guard let httpresponse = response as? HTTPURLResponse,httpresponse.statusCode == 200
                        else
                        {
                            return
                        }
                        
                        // data not null
                        
                        guard let jsonData = data
                        else
                        {
                            return
                        }
                        
                               do
                               {
                                   
                                   
                                   let decoder = JSONDecoder()
                                   let a = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]
                                   print(a as Any)
                                   let newsArticles = try decoder.decode(T.self, from: jsonData)

                                   completionHandler(.success(newsArticles))
                               }
                               catch let error
                               {
                                   completionHandler(.failure(error))
                                print("Error during JSON serialization: \(error.localizedDescription)")
                               }
                       }
            dataTask.resume()
        }
    }
}

 class RequestFactory {
    
    enum Method: String {
        case GET
        case POST
        case PUT
        case DELETE
        case PATCH
    }
    
     static func request(method: Method, url: URL) -> URLRequest {
        var request = URLRequest(url: url)
         request.httpMethod = method.rawValue
       // request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
}

