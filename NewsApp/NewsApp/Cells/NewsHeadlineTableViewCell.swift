//
//  NewsHeadlineTableViewCell.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import UIKit
import SDWebImage

class NewsHeadlineTableViewCell: UITableViewCell {
    
    @IBOutlet weak var NewsImage : UIImageView!
    @IBOutlet weak var NewsTitle : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func layoutSubviews() {
//        super.layoutSubviews()
//        //set the values for top,left,bottom,right margins
//        let margins = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
//        contentView.frame = contentView.frame.inset(by: margins)
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //setting the tableview values for ViewModel
    func configure(for vm: ArticleViewModel) {
        if let url = vm.newsImage{
            NewsImage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder.png"))
        }
            self.NewsTitle.text = vm.newsTitle
        }
    
}
