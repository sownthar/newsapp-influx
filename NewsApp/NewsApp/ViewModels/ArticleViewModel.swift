//
//  ArticleViewModel.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import Foundation

struct ArticleListViewModel {
    let articles: [NewsModel.Article]
    
    init(_ article: [NewsModel.Article]) {
        self.articles = article
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.articles.count
    }
    
    func articleAtIndex(_ index: Int) -> ArticleViewModel {
        let article = articles[index]
        return ArticleViewModel(article)
    }
}

extension ArticleListViewModel {
    
}

struct ArticleViewModel {
    private let article: NewsModel.Article
    
    init(_ article: NewsModel.Article) {
        self.article = article
    }
    
    var newsImage: String? {
        return self.article.urlToImage ?? ""
        }
        
        var newsTitle: String {
            return self.article.title ?? ""
        }
    
    var articleId: String {
        return self.article.source?.id ?? ""
    }
    
    var articleurl: String {
        return self.article.url ?? ""
    }
}




