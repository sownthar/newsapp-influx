//
//  ArticleModel.swift
//  NewsApp
//
//  Created by sowntharrajan s on 27/08/22.
//

import Foundation
struct NewsModel: Decodable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]?
    
    enum CodingKeys: String, CodingKey {

        case status = "status"
        case totalResults = "totalResults"
        case articles = "articles"
       
    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults)
//        articles = try values.decodeIfPresent([Article].self, forKey: .articles)
//    }
    // MARK: - Article
    struct Article: Decodable {
        let source: Source?
        let author: String?
        let title, articleDescription: String?
        let url: String?
        let urlToImage: String?
        let publishedAt: String?
        let content: String?

        enum CodingKeys: String, CodingKey {
            case source, author, title
            case articleDescription = "description"
            case url, urlToImage, publishedAt, content
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            source = try values.decodeIfPresent(Source.self, forKey: .source)
//            author = try values.decodeIfPresent(String.self, forKey: .author)
//            title = try values.decodeIfPresent(String.self, forKey: .title)
//            articleDescription = try values.decodeIfPresent(String.self, forKey: .articleDescription)
//            url = try values.decodeIfPresent(String.self, forKey: .url)
//            urlToImage = try values.decodeIfPresent(String.self, forKey: .urlToImage)
//            publishedAt = try values.decodeIfPresent(String.self, forKey: .publishedAt)
//            content = try values.decodeIfPresent(String.self, forKey: .content)
//
//        }
    }

    // MARK: - Source
    struct Source: Decodable {
        let id: String?
        let name: String?
        
        enum CodingKeys: String, CodingKey {
            case id, name
        }
        
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            id = try values.decodeIfPresent(String.self, forKey: .id)
//            name = try values.decodeIfPresent(String.self, forKey: .name)
//        }
    }
}
// MARK: - Welcome
struct LikesModel: Codable {
    let likes: Int?
    enum CodingKeys: String, CodingKey {
        case likes

    }
    
}
struct CommentsModel: Codable {
    let comments: Int?
    enum CodingKeys: String, CodingKey {
        case comments
    }
}

